import timeit
setup= '''
superSet = set(["Bortman","Sooperman", "Green Torch", "The Avengers", "Insect-Man", "Wing dude"])

superList = ["Bortman","Sooperman", "Green Torch", "The Avengers", "Insect-Man", "Wing dude"]
'''
print(round(sum(timeit.Timer('"Insect-Man" in superSet',setup=setup).repeat(10000,1000)),5))
print(round(sum(timeit.Timer('"Insect-Man" in superList',setup=setup).repeat(10000,1000)),5))
