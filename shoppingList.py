shoppingList = ["thing", "other thing"]
print(shoppingList)
shoppingList.append("new thing")
print(shoppingList)
shoppingList.remove("thing")
print(shoppingList)
del shoppingList[:]
print(shoppingList)
action = input("What would you like to do? (add, remove, clear, quit) ")
while action != "quit":
    if action.split()[0] == "add":
        shoppingList.append(action.split()[1])
        print(shoppingList)
    elif action.split()[0] == "remove":
        try:
            shoppingList.remove(action.split()[1])
        except (ValueError):
            print("That's not in your list")
        print(shoppingList)
    elif action.split()[0] == "clear":
        del shoppingList[:]
        print(shoppingList)
    action = input("What would you like to do? (add, remove, clear, quit) ")
