def takeOut(input, int):
  output = []
  for i in range(len(input)):
    if i%int != 0:
      output.append(input[i])
  return output

def noDup(input):
  input.sort()
  output = []
  for i in range(len(input)):
    if input[i] not in output:
      output.append(input[i])
  return output

def split(input):
  half = int(round(len(input)/2,0))
  return input[:half], input[half:]

print("takeOut")
print(takeOut(list(range(10)), 4))
print("noDup")
print(noDup([1,2,2,3,3,4,4,5,2,3,1,2]))
print("split")
print(split([1,2,3,4,5,6]))
