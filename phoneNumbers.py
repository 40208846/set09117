phonebook = {'Tony Stark':348723982, 'Mr. The Hulk':234032984, 'The Widow One':3432423982, 'Bow-Man':3765434567, 'Thor': 2}

action = input("What would you like to do? (add, remove, lookup, quit) ")
while action != "quit":
    if action == "add":
      name = input("What is their name? ")
      number = input("What is their number? ")
      phonebook[name] = number
     
    elif action == "remove":
        try:
            name = input("What is their name? ")
            del phonebook[name]
        except (KeyError):
            print("They're not in your phonebook")
    elif action == "lookup":
        try:
            name = input("What is their name? ")
            print(phonebook[name])
        except (KeyError):
            print("They're not in your phonebook")
    action = input("What would you like to do? (add, remove, lookup, quit) ")
