board = [[" "," "," "],[" "," "," "],[" "," "," "]]

def printBoard():
    for row  in board:
        rowStr = ""
        for tile in row:
                rowStr += tile
        print(rowStr)

print("X First")
turn = 0
moves = []
move = input("Enter move i.e. 0 1 ")
while move != "quit":
    moves.append([move],turn % 2)

    x = int(move.split()[0])
    y = int(move.split()[1])
    
    if board[x][y] == " ":
        if turn % 2 == 0:
            board[x][y] = "X"
        else:
            board[x][y] = "O"
    else:
        print("Space is full")
        turn -= 1
    printBoard()
    turn += 1
    move = input("Enter move i.e. 0 1 ")

